﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

namespace BusinessManagement {
    public class ExifData {
        private string _value;

        public string Value {
            get { return _value; }
            set { _value = value; }
        }

        private string _label;

        public string Label {
            get { return _label; }
            set { _label = value; }
        }
        
    }
    
    public class Tools {
        public List<ExifData> getExf(Image img) {
            PropertyItem[] propItems = img.PropertyItems;

            List<ExifData> res = new List<ExifData>();
            res.Add(new ExifData() {
                Label = "Resolution X",
                Value =  System.Text.Encoding.ASCII.GetString(propItems[282].Value)
            });
            res.Add(new ExifData() {
                Label = "Resolution Y",
                Value = System.Text.Encoding.ASCII.GetString(propItems[283].Value)
            });
            res.Add(new ExifData() {
                Label = "Ouverture",
                Value = System.Text.Encoding.ASCII.GetString(propItems[37378].Value)
            });
            res.Add(new ExifData() {
                Label = "Temps d'exposition",
                Value = System.Text.Encoding.ASCII.GetString(propItems[33434].Value)
            });
            res.Add(new ExifData() {
                Label = "Iso",
                Value = System.Text.Encoding.ASCII.GetString(propItems[34855].Value)
            });
            return res;
        }
    }
}
