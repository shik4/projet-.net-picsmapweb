﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessManagement {
    public class Evaluation {
        public static void createEval(long photoId, string login, short note) {
            try {
                DataAccess.EvaluationAcces.createEval(photoId, login, note);
            } catch (Exception e) {
                
                throw new BusinessManagementException("Internal Server Error", e);
            }
        }

        public static void deleteEval(long photoId, string login) {
            try {
                DataAccess.EvaluationAcces.deleteEval(photoId, login);
            } catch (Exception e) {

                throw new BusinessManagementException("Internal Server Error", e);
            }
        }

        public static double findEvalsForPicture(long photoId) {
            try {
                return DataAccess.EvaluationAcces.findEvalsForPicture(photoId);
            } catch (Exception e) {
                throw new BusinessManagementException("Internal Server Error", e);
            }
        }

        public static bool hasEvaluate(long photoId, string login) {
            try {
                return DataAccess.EvaluationAcces.hasEvaluate(photoId, login);
            } catch (Exception e) {
                throw new BusinessManagementException("Internal Server Error", e);
            }
        }
    }
}
