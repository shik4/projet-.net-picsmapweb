﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace BusinessManagement {
    public class DonneesExif {
        public static void createExif(Image img, long photoId) {
            try {

                List<ExifData> exifSelec = new Tools().getExf(img);
                foreach (ExifData exifData in exifSelec) {
                    DataAccess.DonneesExifAcces.createExif(exifData.Label, exifData.Value, photoId);
                }
                
            } catch (Exception e) { 
                throw new BusinessManagementException("Internal server error", e);
            }
        }

        public static DataAccess.Donnes_Exif searchTag(string label, long photoId) {
            try {
                return DataAccess.DonneesExifAcces.searchTag(label, photoId);
            } catch (Exception e) {
                throw new BusinessManagementException("Internal server error", e);
            }
        }
    }
}
