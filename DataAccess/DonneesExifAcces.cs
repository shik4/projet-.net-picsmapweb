﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess {
    public class DonneesExifAcces {
        public static void createExif(string label, string value, long photoId) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                Photo pht = (from p in bdd.Photos
                            where p.Id == photoId
                            select p).FirstOrDefault();

                Donnes_Exif toAdd = new Donnes_Exif() {
                    Label = label,
                    Value = value,
                    Photo = pht
                };

                bdd.AddToDonnes_Exif(toAdd);
                bdd.SaveChanges();
            }
        }

        public static Donnes_Exif searchTag(string label, long photoId) {
            using (PicsMapWebEntities bdd = new PicsMapWebEntities()) {
                return (from e in bdd.Donnes_Exif.Include("Photo")
                        where e.Photo.Id == photoId &&
                                e.Label == label
                        select e).FirstOrDefault();
                    }
        }
    }
}
