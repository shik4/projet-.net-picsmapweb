﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using PicsMapWeb.Models;
using System.Security.Cryptography;
using System.IO;

namespace PicsMapWeb.Controllers
{
    public class AccountController : Controller
    {

        // Page affichant une liste des utilisateurs que je suis
        public ViewResult Followers(List<DataAccess.Membership> listSearchUsers)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);
            
            ViewBag.cptName = cpt.Nom;
            ViewBag.cptFirstname = cpt.Prenom;
            ViewBag.cptLogin = cpt.Login;
            ViewBag.cptEmail = cpt.Email;

            if (listSearchUsers == null)
            {
                ViewBag.listFollowers = cpt.CompteSuivi;
            }
            else
            {
                ViewBag.listFollowers = listSearchUsers;
            }

            return View();
        }

        [HttpPost]
        public ActionResult Followers(SearchModel model)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            if (ModelState.IsValid)
            {
                try
                {
                    List<DataAccess.Membership> listSearchUsers1 = new List<DataAccess.Membership>();
                    listSearchUsers1 = BusinessManagement.Compte.findByKey(0, 20, model.Login, cpt.CompteSuivi.ToList());

                    ViewBag.cptName = cpt.Nom;
                    ViewBag.cptFirstname = cpt.Prenom;
                    ViewBag.cptLogin = cpt.Login;
                    ViewBag.cptEmail = cpt.Email;

                    ViewBag.listFollowers = listSearchUsers1;


                    return PartialView("_FollowerList");
                }
                catch (BusinessManagement.BusinessManagementException e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }

            return PartialView("_MyFollowerList"); ;
        }

        public ActionResult DoFollow(string login)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            ViewBag.cptName = cpt.Nom;
            ViewBag.cptFirstname = cpt.Prenom;
            ViewBag.cptLogin = cpt.Login;
            ViewBag.cptEmail = cpt.Email;

            // liste d'abonnés & d'abonnements
            ViewBag.listFollowers = cpt.CompteSuiveur;
            ViewBag.listMyFollowers = cpt.CompteSuivi;

            BusinessManagement.Compte.followCompte(cpt.Login, login);

            return RedirectToAction("Followers");
        }

        public ActionResult DoUnfollow(string login)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            ViewBag.cptName = cpt.Nom;
            ViewBag.cptFirstname = cpt.Prenom;
            ViewBag.cptLogin = cpt.Login;
            ViewBag.cptEmail = cpt.Email;

            // liste d'abonnés & d'abonnements
            ViewBag.listFollowers = cpt.CompteSuiveur;
            ViewBag.listMyFollowers = cpt.CompteSuivi;

            BusinessManagement.Compte.unFollowCompte(cpt.Login, login);

            return RedirectToAction("Followers");
        }

        public ActionResult DoFollowRedirectMyFollowers(string login)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            ViewBag.cptName = cpt.Nom;
            ViewBag.cptFirstname = cpt.Prenom;
            ViewBag.cptLogin = cpt.Login;
            ViewBag.cptEmail = cpt.Email;

            // liste d'abonnés & d'abonnements
            ViewBag.listFollowers = cpt.CompteSuiveur;
            ViewBag.listMyFollowers = cpt.CompteSuivi;

            BusinessManagement.Compte.followCompte(cpt.Login, login);

            return RedirectToAction("MyFollowers");
        }

        public ActionResult DoUnfollowRedirectMyFollowers(string login)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            ViewBag.cptName = cpt.Nom;
            ViewBag.cptFirstname = cpt.Prenom;
            ViewBag.cptLogin = cpt.Login;
            ViewBag.cptEmail = cpt.Email;

            // liste d'abonnés & d'abonnements
            ViewBag.listFollowers = cpt.CompteSuiveur;
            ViewBag.listMyFollowers = cpt.CompteSuivi;

            BusinessManagement.Compte.unFollowCompte(cpt.Login, login);

            return RedirectToAction("MyFollowers");
        }

        // Page affichant la liste des utilisateurs qui me suivent
        public ViewResult MyFollowers(List<DataAccess.Membership> listSearchUsers)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            ViewBag.cptName = cpt.Nom;
            ViewBag.cptFirstname = cpt.Prenom;
            ViewBag.cptLogin = cpt.Login;
            ViewBag.cptEmail = cpt.Email;

            ViewBag.listFollowers = cpt.CompteSuivi;
            if (listSearchUsers == null)
            {
                ViewBag.listMyFollowers = cpt.CompteSuiveur;
            }
            else 
            {
                ViewBag.listMyFollowers = listSearchUsers;
                
            }
            

            return View();
        }

        [HttpPost]
        public PartialViewResult MyFollowers(SearchModel model)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            if (ModelState.IsValid)
            {
                try
                {
                    List<DataAccess.Membership> listSearchUsers1 = new List<DataAccess.Membership>();
                    listSearchUsers1 = BusinessManagement.Compte.findByKey(0, 20, model.Login, cpt.CompteSuiveur.ToList());

                    ViewBag.cptName = cpt.Nom;
                    ViewBag.cptFirstname = cpt.Prenom;
                    ViewBag.cptLogin = cpt.Login;
                    ViewBag.cptEmail = cpt.Email;

                    ViewBag.listFollowers = cpt.CompteSuivi;

                    ViewBag.listMyFollowers = listSearchUsers1;

                    return PartialView("_MyFollowerList");
                }
                catch (BusinessManagement.BusinessManagementException e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }
            return PartialView("_MyFollowerList");
        }

        public ActionResult EditProfile()
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            ViewBag.cpt = cpt;

            // nombre d'abonnés & d'abonnements
            ViewBag.nbFollowers = cpt.CompteSuiveur.Count;
            ViewBag.nbMyFollowers = cpt.CompteSuivi.Count;
            

            // liste d'abonnés & d'abonnements
            ViewBag.listFollowers = cpt.CompteSuiveur;
            ViewBag.listMyFollowers = cpt.CompteSuivi;

            ViewBag.nbPhotos = cpt.Photos.Count;

            // Photo
            if (cpt.Photo_Profil == null)
                ViewBag.UserPhotoUrl = "/Content/i/photo.jpg";
            else
                ViewBag.UserPhotoUrl = cpt.Photo_Profil;

            return View();
        }

        public ActionResult UpdateUserSuccess()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EditProfile(EditUserModel model)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            if (ModelState.IsValid)
            {
                try
                {   
                    DataAccess.Membership cptToUpdate = new DataAccess.Membership();

                    cptToUpdate.Id = cpt.Id;
                    cptToUpdate.Prenom = model.UserName;
                    cptToUpdate.Nom = model.Name;
                    cptToUpdate.Login = model.Login;
                    cptToUpdate.Email = model.Email;
                    cptToUpdate.Mdp = cpt.Mdp;
                    cptToUpdate.Photo_Profil = cpt.Photo_Profil;

                    // Gestion de la photo de profil
                    var webHelper = new Helpers.WebImageHelper();
                    var image = webHelper.GetImageFromRequest();
                    if (image != null)
                    {
                        if (image.Width > 500)
                            image.Resize(500, ((500 * image.Height) / image.Width));

                        // Generate file name
                        var filename = Path.GetFileName(image.FileName);
                        Random rand = new Random();
                        filename = String.Format("{0}_{1}_{2}_{3}_{4}", rand.Next(50000) + "Profil",
                            DateTime.Now.Second + DateTime.Now.Minute + DateTime.Now.Hour,
                            DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year, rand.Next(50000), filename);
                        filename = Path.Combine("~/Content/upload", filename);

                        // Save file
                        image.Save(filename);

                        cptToUpdate.Photo_Profil = Url.Content(filename);
                        Session.Add("CurrentUserPhotoUrl", cptToUpdate.Photo_Profil);
                    }


                    BusinessManagement.Compte.UpdateCompte(cptToUpdate, User.Identity.Name);

                    return RedirectToAction("UpdateUserSuccess");
                }
                catch (BusinessManagement.BusinessManagementException e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }

            // If we got this far, something failed, redisplay form
            return Show((int)cpt.Id);
        }

        public ActionResult Search(List<DataAccess.Membership> listSearchUsers)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            ViewBag.cptName = cpt.Nom;
            ViewBag.cptFirstname = cpt.Prenom;
            ViewBag.cptLogin = cpt.Login;
            ViewBag.cptEmail = cpt.Email;

            // nombre d'abonnés & d'abonnements
            ViewBag.nbFollowers = cpt.CompteSuiveur.Count;
            ViewBag.nbMyFollowers = cpt.CompteSuivi.Count;

            // liste d'abonnés & d'abonnements
            ViewBag.listFollowers = cpt.CompteSuiveur;
            ViewBag.listMyFollowers = cpt.CompteSuivi;

            List<long> idList = new List<long>();
            foreach (var item in cpt.CompteSuivi)
                idList.Add(item.Id);
            ViewBag.listMyFollowersId = idList;

            ViewBag.listSearchUsers = listSearchUsers;

            return View();
        }


        [HttpPost]
        public ActionResult Search(SearchModel model)
        {
            DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);

            if (ModelState.IsValid)
            {
                try
                {
                    List<DataAccess.Membership> listSearchUsers1 = new List<DataAccess.Membership>();
                    listSearchUsers1 = BusinessManagement.Compte.findByKey(0, 20, model.Login);

                    ViewBag.cptName = cpt.Nom;
                    ViewBag.cptFirstname = cpt.Prenom;
                    ViewBag.cptLogin = cpt.Login;
                    ViewBag.cptEmail = cpt.Email;

                    // nombre d'abonnés & d'abonnements
                    ViewBag.nbFollowers = cpt.CompteSuiveur.Count;
                    ViewBag.nbMyFollowers = cpt.CompteSuivi.Count;

                    // liste d'abonnés & d'abonnements
                    ViewBag.listFollowers = cpt.CompteSuiveur;
                    ViewBag.listMyFollowers = cpt.CompteSuivi;

                    List<long> idList = new List<long>();
                    foreach (var item in cpt.CompteSuivi)
                        idList.Add(item.Id);
                    ViewBag.listMyFollowersId = idList;

                    ViewBag.listSearchUsers = listSearchUsers1;

                    return PartialView("_Search");
                }
                catch (BusinessManagement.BusinessManagementException e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }

            return PartialView("_Search");
        }

        public ActionResult Show(int idCpt)
        {
            DataAccess.Membership cpt = null;
            if (idCpt == -1)
                cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);
            else
                cpt = BusinessManagement.Compte.findById(idCpt);
            ViewBag.cpt = cpt;

            // nombre d'abonnés & d'abonnements
            ViewBag.nbFollowers = cpt.CompteSuiveur.Count;
            ViewBag.nbMyFollowers = cpt.CompteSuivi.Count;

            // liste d'abonnés & d'abonnements
            ViewBag.listFollowers = cpt.CompteSuiveur;
            ViewBag.listMyFollowers = cpt.CompteSuivi;

            ViewBag.nbPhotos = BusinessManagement.Photo.findForAccount(0, 200, cpt.Login).Count;

            List<DataAccess.Photo> myListPhotos = new List<DataAccess.Photo>();
            myListPhotos = BusinessManagement.Photo.findForAccount(0, 10, cpt.Login);
            ViewBag.photosList = myListPhotos;


            Dictionary<string, PhotoSerialized> PhotosCopy = new Dictionary<string, PhotoSerialized>();
            List<PhotoSerialized> MarkersCopy = new List<PhotoSerialized>();
            foreach (DataAccess.Photo photo in ViewBag.photosList)
            {
                DataAccess.Membership owner = BusinessManagement.Compte.findById((int)photo.cpt_Id);
                PhotoSerialized p = new PhotoSerialized();
                p.Titre = photo.Titre;
                p.longitude = photo.longitude;
                p.latitude = photo.latitude;
                p.Url = photo.Url;
                p.Description = photo.Description;
                p.date_Poste = ((DateTime)photo.date_Poste).ToString("MM/dd/yyyy");
                p.login = owner.Login;
                MarkersCopy.Add(p);
                PhotosCopy.Add(photo.Id.ToString(), p);
            }
            System.Web.Script.Serialization.JavaScriptSerializer oSerializer =
            new System.Web.Script.Serialization.JavaScriptSerializer();

            ViewBag.photoListJS = oSerializer.Serialize(PhotosCopy);

            ViewBag.markerListJS = oSerializer.Serialize(MarkersCopy);

            return View();
        }

        //
        // GET: /Account/LogOn

        public ActionResult LogOn()
        {
            return View();
        }

        //
        // POST: /Account/LogOn

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.UserName, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

 
        public ActionResult Register(string firstname, string lastname, string login, string email)
        {
            ViewBag.firstname = firstname;
            ViewBag.lastname = lastname;
            ViewBag.login = login;
            ViewBag.email = email;

            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user

                try {
                    // Gestion de la photo de profil
                    var urlImage = "/Content/i/photo.jpg";
                    var webHelper = new Helpers.WebImageHelper();
                    var image = webHelper.GetImageFromRequest();
                    if (image != null)
                    {
                        if (image.Width > 500)
                            image.Resize(500, ((500 * image.Height) / image.Width));

                        // Generate file name
                        var filename = Path.GetFileName(image.FileName);
                        Random rand = new Random();
                        filename = String.Format("{0}_{1}_{2}_{3}_{4}", rand.Next(50000) + "Profil",
                            DateTime.Now.Second + DateTime.Now.Minute + DateTime.Now.Hour,
                            DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year, rand.Next(50000), filename);
                        filename = Path.Combine("~/Content/upload", filename);

                        // Save file
                        image.Save(filename);

                        urlImage = Url.Content(filename);
                    }

                    // Creation du user en BDD
                    BusinessManagement.Compte.creerCompte(model.Login, model.Email, model.Name, model.UserName, model.Password, urlImage);

                    // Authentification
                    FormsAuthentication.SetAuthCookie(model.Login, false /* createPersistentCookie */);

                    // Redirection
                    return RedirectToAction("Private", "Home");

                } catch (BusinessManagement.BusinessManagementException e) {
                    ModelState.AddModelError("", e.Message);

                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePassword

        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Account/ChangePassword

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    DataAccess.Membership cpt = BusinessManagement.Compte.findByLogin(User.Identity.Name);
                    cpt.Mdp = model.NewPassword;
                    using (MD5 md5 = MD5.Create()) {
                        cpt.Mdp = DataAccess.CompteAcces.GetMd5Hash(md5, cpt.Mdp);
                    }
                    BusinessManagement.Compte.UpdateCompte(cpt, User.Identity.Name);
                    return RedirectToAction("ChangePasswordSuccess");
                }
                catch (BusinessManagement.BusinessManagementException e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePasswordSuccess

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
