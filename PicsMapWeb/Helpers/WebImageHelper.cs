﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PicsMapWeb.Models;
using System.Web.Helpers;
using System.IO;

namespace PicsMapWeb.Helpers
{
    public class WebImageHelper
    {
        public WebImage GetImageFromRequest()
        {
            var request = System.Web.HttpContext.Current.Request;

            if (request.Files.Count == 0)
                return null;

            try
            {
                var postedFile = request.Files[0];
                var image = new WebImage(postedFile.InputStream)
                {
                    FileName = postedFile.FileName
                };
                return image;
            }
            catch
            {
                // The user uploaded a file that wasn't an image or an image format that we don't understand
                return null;
            }
        }
    }
}
