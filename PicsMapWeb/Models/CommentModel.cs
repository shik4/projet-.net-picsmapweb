﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace PicsMapWeb.Models
{
    public class CommentModel
    {
        [Required]
        [Display(Name = "Description")]
        public string Texte { get; set; }

        [Required]
        [Display(Name = "IdPhoto")]
        public int IdPhoto { get; set; }

        [HiddenInput]
        public int IdAuthor { get; set; }
    }

    public class CommentListModel {
        [Required]
        [Display(Name = "Description")]
        public string Texte { get; set; }

        [Required]
        [Display(Name = "Auteur nom")]
        public string authName { get; set; }

        [Required]
        [Display(Name = "Auteur prénom")]
        public string authFirstname { get; set; }

        [Required]
        [Display(Name = "Date")]
        public DateTime date { get; set; }

        [Required]
        [Display(Name = "Id")]
        public long Id { get; set; }

        [Required]
        [Display(Name = "IdCpt")]
        public long IdCpt { get; set; }

        [Required]
        [Display(Name = "PhotoUserProfil")]
        public string PhotoUserProfil { get; set; }
    }
}