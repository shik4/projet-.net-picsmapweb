﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace PicsMapWeb.Models
{
    public class FilterModel
    {
        public string Filter { get; set; }
        public IEnumerable<SelectListItem> Filters
        {
            get
            {
                List<SelectListItem> filterList = new List<SelectListItem>();

                filterList.Add(new SelectListItem
                {
                    Text = "Les dernières photos postées",
                    Value = "1"
                });
                filterList.Add(new SelectListItem
                {
                    Text = "Les dernières photos de mes amis",
                    Value = "2"
                });
                filterList.Add(new SelectListItem
                {
                    Text = "Les photos de mes amis",
                    Value = "3"
                });
                filterList.Add(new SelectListItem
                {
                    Text = "Mes photos",
                    Value = "4"
                });

                return filterList;
            }
        }
        public string tagValue { get; set; }
    }
}